var request = require('request');
var client = function(credentials){
    this.host     = credentials.host;
    this.username = credentials.username;
    this.password = credentials.password;
}

    /**
     * prepare and send post request
     * @param data
     * @param callback
     */
    client.prototype.create_request = function (data, callback) {
        var self = this;
        data.username = this.username;
        data.password = this.password;
        request.post(
            self.host,
            {form: data},
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    var convertedResult = self.prepareResponse(body);
                    if(convertedResult.errorFound==1){
                        callback(convertedResult.errorMessage,false);
                    } else {
                        callback(false,convertedResult);
                    }
                } else {
                    callback('Such host not found',false);
                }
            }
        );
    }

    /**
     * convert string to json
     * @param data
     * @returns {{}}
     */
    client.prototype.prepareResponse = function (data) {
        data = data.split('&');
        var result = {};
        for (var i in data) {
            var res = data[i].split('=');
            var val = '';
            for (var j in res) {
                if (j > 0) {
                    val += res[j];
                }
            }
            result[res[0]] = val;
        }
        return result;
    }

    /**
     * api calls
     */
    client.prototype.NewProspect = function (data, callback) {
    data.method = 'NewProspect';
    this.create_request(data, callback);
    },
    client.prototype.NewOrder = function (data, callback) {
        data.method = 'NewOrder';
        this.create_request(data, callback);
    },
    client.prototype.NewOrderCardOnFile = function (data, callback) {
        data.method = 'NewOrderCardOnFile';
        this.create_request(data, callback);
    },
    client.prototype.CreateRequest = function (data, callback) {
        this.create_request(data, callback);
    }
module.exports = client;